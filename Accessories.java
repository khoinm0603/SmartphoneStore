
package smartphonestore;



public class Accessories extends Product {
    private String type ; 

    public Accessories() {
    }

    public Accessories(String id, String name, String trademark, int price, int quantity, String img,String type) {
        super(id, name, trademark, price, quantity, img);
        this.type = type;
    }
    public String getInfoForChild() {
        return type;
    }

    public void setInfoForChild(String type) {
        this.type = type;
    }

    
    @Override
    public String toString() {
        return super.toString()+ ","+getInfoForChild() ;
    }

    
}
