package smartphonestore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.nio.file.Files.list;
//import java.util.ArrayList;
//import java.util.Collections;
//import static java.util.Collections.list;
//import java.util.List;
//import java.util.Scanner;
import java.util.*;

public class ProductManage {
    ArrayList<Product> productList = new ArrayList<>();
    Product product = new Product();
    Smartphone smartphone = new Smartphone();
    Accessories accessories = new Accessories();

    //-----------------------------------------------------------------------------//
    public void fileReader(String filename) {
        try {
            File f = new File(filename);
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);

            String ln = "";
            while ((ln = br.readLine()) != null) {
                if (ln.trim().equals(" ")) {
                    continue;
                }
                String[] arr = ln.split(",");
                //System.out.println(arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3] + " " + arr[4] + " " + arr[5] + " " + arr[6] + " ");
                if (arr[0].contains("sm")) {
                    productList.add(new Smartphone(arr[0], arr[1], arr[2], Integer.parseInt(arr[3]), Integer.parseInt(arr[4]), arr[5], arr[6]));
                } else {
                    productList.add(new Accessories(arr[0], arr[1], arr[2], Integer.parseInt(arr[3]), Integer.parseInt(arr[4]), arr[5], arr[6]));
                }
//                        System.out.println(productList.get(0).toString());

            }
            br.close();
            fr.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String inputID() {
        Scanner sc = new Scanner(System.in);
        String idClassify;
        int temp = 0;
        do {
            if (temp == 0) {
                System.out.println("Hay nhap ma san pham  ");
            } else {
                System.out.println("Hay nhap lai ma san pham  ");
            }
            System.out.println("Dien thoai thi ma san pham phai co chu 'sm'");
            System.out.println("Phu kien thi ma san pham phai co chu 'ac'");
            idClassify = sc.nextLine();
            temp++;
        } while (!idClassify.contains("sm") && !idClassify.contains("ac"));
        return idClassify;
    }



    public void inputInfoProduct() {
        Scanner sc = new Scanner(System.in);

        while(true){
            try{
                String id = inputID();
                product.setId(id);
                System.out.println("Hay nhap ten san pham");
                product.setName(sc.nextLine());
                System.out.println("Hay nhap gia san pham");
                product.setPrice(Integer.parseInt(sc.nextLine()));
                System.out.println("Hay nhap luong ton san pham");
                product.setQuantity(Integer.parseInt(sc.nextLine()));
                System.out.println("Hay nhap ten thuong hieu");
                product.setTrademark(sc.nextLine());
                System.out.println("Hay nhap anh san pham");
                product.setImg(sc.nextLine());
                product.setInfoForChild(sc.nextLine());
                if (id.contains("sm")) {
                    smartphone.setInfoForChild(inputFeature(id));
                    productList.add(new Smartphone(product.getId(), product.getName(), product.getTrademark(),
                            product.getPrice(), product.getQuantity(),
                            product.getImg(), smartphone.setInfoForChild()));
                } else {
                    accessories.setInfoForChild(inputFeature(id));
                    productList.add(new Accessories(product.getId(), product.getName(),
                            product.getTrademark(), product.getPrice(), product.getQuantity(),
                            product.getImg(), accessories.getInfoForChild()));
                }
                break;
            }catch(NumberFormatException e){System.out.println("Vui long nhap lai");}
        }
        

    }

    public void output(ArrayList<Product> productListO) {
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();

        for (int i = 0; i < productListO.size(); i++) {

            if (productListO.get(i).getId().contains("sm")) {
                smT = (Smartphone) productListO.get(i);
                System.out.printf("%4s%50s%30s%10d%10d%10s%40s\n", smT.getId(), smT.getName(), smT.getTrademark(), smT.getPrice(), smT.getQuantity(), smT.getImg(), smT.getInfoForChild());
            } else if (productListO.get(i).getId().contains("ac")) {
                acT = (Accessories) productListO.get(i);
                System.out.printf("%4s%50s%30s%10d%10d%10s%40s\n", acT.getId(), acT.getName(), acT.getTrademark(), acT.getPrice(), acT.getQuantity(), acT.getImg(), acT.getInfoForChild());
            }
        }
    }

    void menuSearch() {
        Scanner sc = new Scanner(System.in);
        int choice;
        String id;
        String cont;
        ArrayList<Product> productLT = new ArrayList<>();
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();
        do {
            System.out.println("Ban muon tim san pham co cung thong tin nao sao day:");
            System.out.println("1:ID");
            System.out.println("2:ten san pham");
            System.out.println("3:ten thuong hieu");
            choice = Integer.parseInt(sc.nextLine());
            switch (choice) {
                case 1:
                    productLT = searchId();
                    break;
                case 2:
                    productLT = searchTrademark();
                    break;
                case 3:
                    productLT = searchName();
                    break;
                default:
                    System.out.println("Ban da nhap sai chuc nang");
            }
            if (productLT.isEmpty()) {
                System.out.println("Khong co san pham ma ban muon tim");
            } else {
                output(productLT);
                System.out.println("Ban co muon chinh sua danh sach san pham(y/n)");
                String temp = sc.nextLine();
                if(temp.equalsIgnoreCase("y")){
                    adjust(productLT);
                }
            }
            System.out.println("Ban co muon tiep tuc tim kiem san pham(y/n)");
            cont = sc.nextLine();
        } while (cont.equalsIgnoreCase("y"));
    }

    public ArrayList<Product> searchTrademark() {
        Scanner sc = new Scanner(System.in);
        ArrayList<Product> productListT = new ArrayList<>();
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();
        System.out.println("Vui long nhap ten thuong hieu");
        String trademark = sc.nextLine();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getTrademark().equalsIgnoreCase(trademark)) {
                productListT.add(productList.get(i));
            }
        }
        
        if (productListT.isEmpty()) {
            for (int i = 0; i < productList.size(); i++) {
                if (productList.get(i).getTrademark().contains(trademark)) {
                    productListT.add(productList.get(i));
                }
            }
        }
        return productListT;
    }

    public ArrayList<Product> searchName() {
        Scanner sc = new Scanner(System.in);
        ArrayList<Product> productListT = new ArrayList<>();
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();
        System.out.println("Vui long nhap ten san pham");
        String name = sc.nextLine();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getName().equalsIgnoreCase(name)) {
                productListT.add(productList.get(i));
            }
        }
        System.out.println(productListT.isEmpty()+"1");
        if (productListT.isEmpty()) {
            for (int i = 0; i < productList.size(); i++) {
                if (productList.get(i).getName().contains(name)) {
                    productListT.add(productList.get(i));
                }
            }
        }
        
        return productListT;
    }

    public ArrayList<Product> searchId() {
        Scanner sc = new Scanner(System.in);
        ArrayList<Product> productListT = new ArrayList<>();
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();
        System.out.println("Vui long nhap ma san pham");
        String id = sc.nextLine();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getId().equalsIgnoreCase(id)) {
                productListT.add(productList.get(i));
            }
        }
        if (productListT.isEmpty()) {
            for (int i = 0; i < productList.size(); i++) {
                if (productList.get(i).getId().contains(id)) {
                    productListT.add(productList.get(i));
                }
            }

        }
        return productListT;
    }

    private void adjust(ArrayList<Product> productListAd) {
        Scanner sc = new Scanner(System.in);
        String cont = null, cont1;
        int choice, choice1;
        int indexL = 0;
        Smartphone smT = new Smartphone();
        Accessories acT = new Accessories();
        
        boolean flag=false;
        do {
            if(flag){
            output(productListAd);
            flag = true ;
            }
            System.out.println("Chon chuc nang ban muon thuc hien");
            System.out.println("1: Sua san pham");
            System.out.println("2: Xoa san pham");
            choice = Integer.parseInt(sc.nextLine());
            if (choice == 1) {
                System.out.println("Chon san pham ban muon chinh sua(Theo ID)");
                String id = sc.nextLine();
                for (int i = 0; i < productList.size(); i++) {
                    if (productList.get(i).getId().equalsIgnoreCase(id)) {
                        if (productList.get(i).getId().contains("sm")) {
                            smT = (Smartphone) productList.get(i);
                            //System.out.println(smT.toString());
                        } else {
                            acT = (Accessories) productList.get(i);
                            //System.out.println(acT.toString());
                        }
                        indexL = i;
                    }
                }
                do {

                    System.out.println("Chon thong tin cua san pham ma ban muon sua ");
                    System.out.println("1:ID");
                    System.out.println("2:ten san pham");
                    System.out.println("3:ten thuong hieu");
                    System.out.println("4:gia");
                    System.out.println("5:luong ton kho");
                    System.out.println("6:anh san pham");
                    System.out.println("7:cpu/tinh nang them");
                    choice1 = Integer.parseInt(sc.nextLine());
                    switch (choice1) {
                        case 1:
                            productList.get(indexL).setId(inputID());
                            break;
                        case 2:
                            System.out.println("Nhap ten san pham moi ");
                            productList.get(indexL).setName(sc.nextLine());
                            break;
                        case 3:
                            System.out.println("Nhap ten thuong hieu moi");

                            productList.get(indexL).setTrademark(sc.nextLine());
                            break;
                        case 4:
                            System.out.println("Nhap gia moi");
                            while (true) {
                                try {
                                    productList.get(indexL).setPrice(Integer.parseInt(sc.nextLine()));
                                    break;

                                } catch (NumberFormatException e) {
                                    System.out.println("Vui long nhap so vao gia san pham");
                                }
                            }
                        case 5:
                            System.out.println("Nhap so luong ton kho moi");
                            while (true) {
                                try {
                                    productList.get(indexL).setQuantity(Integer.parseInt(sc.nextLine()));
                                    break;
                                } catch (NumberFormatException e) {
                                    System.out.println("Vui long nhap so vao so luong ton kho moi san pham");
                                }
                            }
                            break;
                        case 6:
                            System.out.println("Nhap anh san pham moi");
                            productList.get(indexL).setImg(sc.nextLine());
                            break;
                        case 7:
                            if (productList.get(indexL).getId().contains("sm")) {
                                System.out.println("Nhap cpu moi");
                                smT = (Smartphone) productList.get(indexL);
                                smT.setInfoForChild(sc.nextLine());
                            } else {
                                System.out.println("Nhap tinh nang them moi");
                                acT = (Accessories) productList.get(indexL);
                                acT.setInfoForChild(sc.nextLine());
                                break;
                            }
                        default:
                            System.out.println("Khong co san pham can tim");
                            break;
                    }
                    System.out.println("Ban co muon tiep tuc sua thong tin(y/n)");
                    cont1 = sc.nextLine();
                } while (cont1.equalsIgnoreCase("y"));
            } else if (choice == 2) {
                System.out.println("Chon san pham ban muon xoa(Theo ID)");
                String id = sc.nextLine();
                for (int i = 0; i < productList.size(); i++) {
                    if (productList.get(i).getId().equalsIgnoreCase(id)) {
                        productList.remove(productList.get(i));
                        productListAd.remove(productListAd.get(i));
                    }
                }
            } else {
                System.out.println("Khong co chuc nang can tim");
            }
            System.out.println("Dan sach san pham sau khi chinh sua");
            output(productList);
            System.out.println("Ban co muon tiep tuc chinh sua danh sach san pham(y/n)");
            cont = sc.nextLine();
        } while (cont.equalsIgnoreCase("y"));
    }

    public void sort() {
        for (int i = 0; i < productList.size(); i++) {
            Collections.sort(productList);
        }
    }

    public void saveToFile(String filename) throws IOException {
        try {
            File file = new File(filename);
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);

            for (int i = 0; i < productList.size(); i++) {
                pw.write(productList.get(i) + ","+"\n");
            }
            fw.close();
            pw.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
