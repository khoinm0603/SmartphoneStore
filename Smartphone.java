    
package smartphonestore;

import java.util.Scanner;

public class Smartphone extends Product {
    String cpu;

    public Smartphone() {
    }

    public Smartphone(String id, String name, String trademark, int price, int quantity, String img,String cpu) {
        super(id, name, trademark, price, quantity, img);
        this.cpu = cpu;
    }
    public String getInfoForChild() {
        return cpu;
    }

    public void setInfoForChild(String cpu) {
        this.cpu = cpu; 
    }
    
    @Override
    public String toString() {
        return super.toString()+ ","+this.cpu; 
    }
    
    }
