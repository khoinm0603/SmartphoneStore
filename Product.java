
package smartphonestore;
public class Product implements Comparable<Product>{
    private String name ;
    private String id;
    private String trademark;
    private int price;
    private int quantity;
    private String img ;

    public Product() {
    }

    public Product(String id,String name , String trademark, int price, int quantity, String img) {
        this.id = id;
        this.name = name;
        this.trademark = trademark;
        this.price = price;
        this.quantity = quantity;
        this.img = img;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }
    public void setInfoForChild(String Child)//IMPORTANT: This class just use for child not use for this class
    {
    }

    public String getInfoForChild()//IMPORTANT: This class just use for child not use for this class
    {
        return null;
    }
    
    @Override
    public String toString() {
        return this.id+","+this.name+","+this.trademark+","+this.price+","+this.quantity+","+this.img;
    }

    @Override
    public int compareTo(Product pro) {
       return this.id.compareTo(pro.id); 
    }
  
}      

